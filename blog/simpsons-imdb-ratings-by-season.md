---
title: "The Simpsons: An analysis of IMDB ratings by season"
summary: "When did the longest-running series in history jump the shark?"
image: "simpsons"
date: "2023-10-13"
---

The wall came down, and about a month later :SimpsonsXRef[the first episode]{modelName="simpsons-episode" slug="7g08"} of *The Simpsons* aired on the Fox network. For 34 years the show has been locked in a kind of strange timewarp, with characters who never age, while the world around them has changed to be almost unrecognizable. They have outlasted five US presidents (including :SimpsonsXRef[predicting]{modelName="simpsons-episode" slug="babf13"} and then *still being around for* the end of President Trump), and essentially pre-date the (widespread usage of) the internet.

Mat Groening's cynical take on modern American life had a profound impact on the millenial generation, and many of us can effortlessly reference the first few seasons. The show is forever connected with the 90s, and the commonplace understanding is that it [jumped the shark](https://en.wikipedia.org/wiki/Jumping_the_shark) when that decade ended. The :SimpsonsXRef[season 11 finale]{modelName="simpsons-episode" slug="babf19"} always felt like an ending, and while I kept hope alive for another year or so, my interest waned pretty rapidly after that.

The numbers confirm it (people can come up with statistics to prove anything).

::SimpsonsChart{modelName="simpsons-episode"}

The data show user-provided ratings, from [IMDB](https://www.imdb.com/), for each episode (in green). The dotted line shows the median rating for each season, while the grey box contains the middle two quartiles. In other words, half of each season's episodes fall within the grey zone, while the worst quarter of episodes fall below it and the best quarter fall above it.

I took the raw data (stole, made up, what's the difference?) from this [Observable notebook](https://observablehq.com/@observablehq/plot-simpsons-ratings). They represent the first 600 episodes.

A few noteworthy data points. The :SimpsonsXRef[one with the dental plan]{modelName="simpsons-episode" slug="9f15"} is my personal favourite, but although it tops season 4 it isn't the highest of all time. That honor is a tie between :SimpsonsXRef[the one where Homer acquires the Denver Broncos]{modelName="simpsons-episode" slug="3f23"} and the one where :SimpsonsXRef[a power plant employee hilariously electrocutes himself]{modelName="simpsons-episode" slug="4f19"}. The early seasons have a few notable stinkers: :SimpsonsXRef[all]{modelName="simpsons-episode" slug="9f17"} :SimpsonsXRef[clip]{modelName="simpsons-episode" slug="2f33"} :SimpsonsXRef[shows]{modelName="simpsons-episode" slug="5f24"}.

Among later seasons, the 16th season episode :SimpsonsXRef[where Springfield legalizes gay marriage]{modelName="simpsons-episode" slug="gabf04"} (in 2005), caused quite a splash in the media when it aired, but it ranks distinctly mediocre in terms of what audiences thought. The one :SimpsonsXRef[that features the Decemberists]{modelName="simpsons-episode" slug="pabf20"} - the only episode I've made an effort to watch in at least 20 years - fares a little better but isn't a standout.

Meanwhile, there are a handful of episodes from the past two decades that are clearly a cut above the common crowd. They are pretty easy to pick out, and I haven't seen them, so I won't call them out individually.

Most popular TV shows last a few seasons past the point where audiences give up. This show seems exceptional: I don't know who exactly is still watching, but it's nice to know that science confirms that we gave up at the right time.
