---
title: "Clay Shirky's Cognitive Surplus and the Open-Science Movement"
date: "2012-07-01"
---

I recently finished Clay Shirkey's "Cognitive Surplus" and there are a number of great points in here relevant to the open science movement. This might seem a little surprising given the central thesis of the book: for academics, contributing to knowledge creation and dissemination is not really "surplus", it's their job. Nevertheless, the values at the heart of the open source software movement, Wikipedia, and so on, are really the inspiration for open science, with the added kick that if so much has been done by people working for free, a class of professionals paid to contribute to human knowledge should be able to do even more.

Here are some key quotes from the book relevant to open access and open science:

> Curiously, an organization that commits to helping society manage a problem also commits itself to the preservation of that same problem, as its institutional existence hinges on society's continued need for its management.

The examples of this we are currently seeing include the tension between the hospitality industry and AirBNB, and between taxi companies and carpooling apps. Oh, and academic publishers vs open access publishers.

> The great tension in media has always been that freedom and quality are conflicting goals. There have always been people willing to argue that an increase in freedom to publish isn't worth the decrease in average quality.

He goes on to quote Martin Luther and Edgar Allen Poe saying exactly that.

> When publication - the act of making something public - goes from being hard to being virtually effortless, people used to the old system often regard publishing by amateurs as frivolous, as if publishing was an inherently serious activity. In never was , though. Publishing had to be taken seriously when its cost and effort made people take it seriously - if you made too many mistakes, you were out of business. But if these factors collapse, then the risk collapses, too. An activity that once seemed inherently valuable turned out to be only accidentally valuable, as a change in the economics revealed.

Here's the message for us: the decrease in overall quality of the scientific literature IS happening. It's here to stay. Whatever model of peer review we choose, there will always be a place to publish bad papers, because publication itself is cheap as dirt. But this is also an opportunity. Since publication is cheap, we have the chance to simply determine quality post-publication. Publishing itself is no longer the bottleneck it once was, and so the chance to put everything out there and let the record correct itself is now here.
