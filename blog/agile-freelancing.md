---
title: "Being Agile as a Freelancer"
date: "2019-01-07"
---

When I began working a freelancer last year, I knew I wanted to use an agile approach to build apps for my clients, even if they were small projects. As there are limited resources for agiling independently, I thought I would write about my experience for others to use.

First of all, some terms. I am going to contrast **Agile development** with the so-called **waterfall approach**. In the waterfall approach, the entire website or application is envisioned from the beginning. The details vary a bit, but in my experience, it involves four stages: 1) wireframes, 2) mockups, 3) development and 4) deployment. At each stage, the client approves the work so far and the designer or developer moves on to the next stage.

In the agile approach, a working website or app is produced at each stage. While the overall goal or scope is mapped out at the start, a key feature is that change is easy to handle. If a user changes their mind about particular featues as you go along, that feature can be changed without affecting the rest of the app. This is harder with waterfall, because a feature will be "present" in wireframes and mockups, but the client likely won't **fully** understand what it does until it works (is developed).

So how do I do it? I begin by working with a client to map out a set of user stories in order to scope the project. I then seperate the stories into sprints and aim to complete each sprint every week or two. I request the client's feedback and the end of each sprint and we make any changes we need to to the plan.

Without fail, I have found this approach to be better for clients, better for developers, and better for the end-result. Here are the reasons why:

## 1. Your clients stay engaged

Asking clients to review work that is delivered using a waterfall approach means telling them what types of feedback you will accept at what time. If you show them mockups, they will give you feedback on language and copy. If you show them wireframes, they will likely be confused (and berate your poor colour choices). You can get sign-off at each stage only by telling them to ignore whatever doesn't work (yet), and focus on what you want them to.

For this reason, getting approval in a waterfall project always feels more like checking a box than actively involving the client. Only when they are able to fully use a feature will they understand whether you've built what they thought they were asking for.

An obvious counter-argument: some clients don't want to be engaged. They want you to go away and come back with working software. You don't want these clients.

## 2. It's easier to estimate

This may seem counterintuitive since embracing agile means embracing change, and estimates depend on accurate scoping. The key is to look at each sprint as an individual project and estimate based on that. Make sure the client understands your goal is to deliver _working_ software at each sprint, and continuing the project depends on the agreement of both parties. That way, you are delivering a service, not a product.

Granted, most clients will want some assurance that you can build their entire vision for a price they deem reasonable. So it's useful to be able to estimate a large project, assuming there are only minimal changes to the scope as you go along. Hopefully, your clients will understand that changes they request as you go along will generally add to that scope.

The alternative, of course, is to argue with the client every time they ask for a change.

It might seem that the agile approach lends itself only to hourly billing, but this need not be the case. Agreeing to a flat rate for a limited number of sprints is a great way to build trust and get the project off to a good start.

## 3. You keep your clients

Stop me if this sounds familiar: you launch a website for a client and don't hear from them for five years or so. When their website begins to feel dated, they come back asking for a new one, and you start the process over again. That's if you're lucky - if you're unlucky, they find another designer/developer/agency and they just call asking to transfer their domain.

You're delivering a service, not a product. Websites and apps are never done. Schedule some time to look at analytics, and test in the latest browsers after you launch and periodically every year or quarter. Your clients will get the most out of their websites/apps by continuously thinking about how to make them better. This is a win-win.

## 4. Your code is better

Agile forces a particular way of thinking about code. A function is created for one purpose in one sprint; it is later given a new set of responsibilities in a future sprint. Because we have tests (right?), we know we aren't breaking the function's original purpose when we modify it to be more durable. We find bugs and fix them: we add more tests. The durability allows for change: it also makes it easier to find and fix problems. It makes it easier to repurpose units because the units. The code is better because it has been revisited several times.

Freelance projects worked on for a period of time by a single developer are going to be smaller in scale than projects worked on by a team over the course of years. With this difference in scale, it can be tempting to skip tests, ship code, and move on. And this is true, as far as it goes. But it ignores the basic reality that your software **is** going to change. It **will** have bugs. Your clients **will** change their minds, and if the projects get any attention from end users at all, they will have more requests that will lead to more change. Very likely the nature of this change will be to make it larger and more complex - so build for it from the beginning.
