---
title: "Conspiracy Thinking is Not Skepticim"
date: "2019-08-28"
---

It is the Flat Earthers that finally gave me pause.

Not the hypothesis itself. In the spectrum of likely possible realities, I put "flat earth" in between [a world sitting on the back of turtle](https://en.wikipedia.org/wiki/World_Turtle) and [a direct, causal relationship between Nicolas Cage movies and swimming pool drownings](https://dangerousminds.net/comments/spurious_correlations_between_nicolas_cage_movies_and_swimming_pool).

Is there, however, something virtuous in the skepticism that allows one to persist in the belief that the world is flat? I cannot prove it one way or the other, [at least not without going to more trouble that I'm willing to](https://www.wired.com/2014/05/wuwt-foucaults-pendulum/). My relative certainty is based to a large degree on the relative certainty of others: the people who have done the experiments (and the reasoning to complement them). It is the rejection of consensus opinion that represents the conspiracy: the addendum that a large number of people are lying (or very deluded) to preserve a hypothesis that simply cannot be true if the vast majority of experts on that subject reject it.

And yet, skepticism is the foundation of science. "Collective wisdom" was rejected by Darwin, Galileo and Copernicus. How can we tell the difference?

To start, there is nothing fundamentally flawed in proposing a conspiracy. People conspire to do things all the time: there is no reason a hypothesis is false simply because it involves a conspiracy. The point is that the more people a conspiracy would have to involve, the less chance such a reality could continue to persist without anyone speaking out. A world in which all national governments, astronomers, pilots and astronauts are deliberately lying to make us believe the world is round, for no reason, is not impossible. It is just extraordinarily unlikely.

The difference between skepticism and conspiracy thinking is one of humility. Science is, above all, an act of ultimate humility. Science is creative only in the sense that Newton created Newton's laws: the laws would still exist if he had never been born. One's own intelligence, fortitude, conviction and work-ethic, have no bearing whatsoever on the answer.

Darwin, Galileo and Copernicus did not reject collective wisdom without cause. Each of them realized precisely why their key insight contradicted the standing consensus, and - critically - what evidence would overturn their own new theories.

Conspiracy thinking is the opposite of humility. It is a belief that perseveres even in the presence of overwhelming contradictory evidence. It is an assertion of power over objective reality, that simple facts cannot confine the strength of ones' convictions. It is a cliche to say that knowledge is power. But it is also true that the illusion of knowledge is the illusion of power, a simple escape from the tyranny of cold, hard truth. It is also an attempt to disarm those smarmy experts who get to say what is what, simply because they've spent long enough studying the subject that they actually know what they're talking about.

To be fair, the Flat Earthers seem like nice people, and a certain level of distrust in our politicians and institutions is probably healthy. But really, what is the difference between them and the cess-pool dumpster-fire of nutbars [threatening grieving parents](https://www.timesofisrael.com/parents-of-jewish-sandy-hook-victim-forced-to-move-7-times-due-to-harassment/), and [shooting up pizza joints](https://www.esquire.com/news-politics/news/a51268/what-is-pizzagate/), and [killing their children through negligence](https://lawstreetmedia.com/news/alberta-parents-found-guilty-didnt-provide-necessaries-life/)? Only the selection of which truths they choose to reject. And without humility, there simply is no way to tell the difference.
