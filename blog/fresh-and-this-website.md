---
title: 'A Fresh look at server-side vs client-side rendering'
date: 'draft'
---

This website is in part an attempt to explore technology solutions for data-rich blogging and journalism on the web.

I have found that neither traditional server-side nor today's most popular client-side rendering solutions are a good fit for this task. The JavaScript libraries that couple well with server-rendered HTML are not up to the task of providing the level of interactivity I want, and websites built entirely in JavaScript are error-prone and reject the principles of progressive enhancement. Without the testing resources that only very large companies have access to, they usually provide a frustrating experience for users.

JAM stack frameworks are currently the best idea the community has to bridge this gap, but they fall short of server-side frameworks in terms of programming HTTP responses in a flexible way. For a website that requests data and graphs it, dynamically rendered _pages_ and a flexible API should be strict requirements.

In my opinion, _[Fresh](https://fresh.deno.dev/) is one of the best things to happen to the web in a long time._ The vast majority of the content on this side is rendered on the server. Where interactivity is needed, JavaScript and data are delivered and hydrated to existing components. Data graphics can be rendered server-side as SVG and seemlessly re-animated as canvas. Additional data can be requested from an API and appended to the graphic, or entire new pages rendered from the same template.

Fresh is also written in Deno, so it provides TypeScript, JSX, linting, testing and package management with very little configuration. It uses Preact rather than React, drastically decreasing the amount of JavaScript delivered and improving time-to-glass performance.

Other choices I made:
- I'm using CouchDB as a database which gives me neat way to sync my development and production environments. I'm using this to store raw data (for graphs), markdown, and even images. Mostly, I work with content locally on my conputer and "ingested" it into the database using some custom Deno code. I may open-source this at some point in the future if I have time.
- Markdown is converted to an mdast tree at ingestion-time by [unified](https://unifiedjs.com/), and then rendered via a custom Preact component. This actually gives me a way to render interactive elements (eg data graphics) within content by using markdown extensions.
- I use d3 for data manipulation, but stick to Preact for rendering. JSX is truly expressive for data-graphics, and going back to imperative programming for rendering is never going to be a realistic option.
- CSS is currently Fresh's biggest weakness. Although I ended up using [Tailwind](https://tailwindcss.com/), I didn't use Fresh's own plugin because I don't like CSS-in-JS and felt it didn't fit with the other values Fresh was espousing.
- I'm also using ImageMagick to resize images, and it wasn't super-easy to get it to work with Deno. This sort of thing will hopefully become less common as the community matures and Deno gains wider acceptance.

Despite these growing pains, I think Fresh's approach to unifying server-side and client-side rendering is the future. Building this site was a pleasure and I hope to create more like it.
