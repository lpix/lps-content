---
title: "Why the Frontend Keeps Getting Harder"
date: "2020-02-28"
---

I gave a talk recently extolling the virtues of PouchDB and how you can use it build web experiences that feel very much like native apps. In short, with PouchDB you can give users app-like experiences with no login, and that store data while offline. And yet they still have the best feature of web app: they can found with a URL.

After the talk, I got a very natural question:

> Are there any UI libraries for resolving conflicts?

Conflicts are a natural part of PouchDB apps, because if two users edit the same piece of data while offline, it's only natural that any differences have to be resolved.

The question threw me for a minute, because at first I thought: "Hey! There should be UI for resolving conflicts! And this is JavaScript, and it's 2019, if something _should_ be built, surely someone has already done it!"

A few seconds of thought cleared it up for me, though. How you deal with conflicts depends entirely on the nature of the data being editing, and therefore there can be as much variety in conflict UI as there is in the UI for editing values in the first place. How you resolve a conflict in a rich-text document bears no relationship to how you would do it for, say, a phone number, and so the question is really moot.

The full implications of the question didn't really hit me until later, though. Basically, **if we want apps to work offline, we inherit all the problems of distributed computing**, whether PouchDB is our tool of choice or not. Distributed computing, formerly the province of academics, comes into the browser uninvited.

And of course, we've been in this movie before. When the logic of user interaction moved from the server to the client, [frontend developers had to worry about managing application state](thinking-in-state-the-tao-of-frontend-development.html), and we had to invent browser state machines.

With more and more computation happening in the browser, frontend developers had to learn about time-complexity, mostly for the first time in their careers.

When more and more code had to be delivered to the client to perform these additional tasks, a whole new science of build pipelines and code-splitting grew up.

I think about this every time I see yet another blog post about how complicated web development has become. **It's become complicated because it is complicated.**
