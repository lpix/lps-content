---
title: "The complex story of food inflation in Canada"
summary: "Christmas is coming, but the squeeze continues"
image: "food-price-scrn"
date: "2023-12-04"
---

Overall inflation seems to be cooling, but the costs of food and housing continue to soar across Canada. In October 2023, food inflation was still nearly double the overall inflation rate, and the federal governement began steps to [stabilize prices through regulation of the major grocery chains](https://www.cbc.ca/news/politics/grocery-chains-promise-more-discounts-price-freezes-1.6987787).

Exactly what items are behind the increased price at the register depends on where you live. In :FoodPriceSpan[British Columbia, potatoes are more than twice the price]{modelName="food-price-history" slug="british-columbia-potatoes-per-kilogram"} they were in 2017, while in :FoodPriceSpan[Prince Edward Island, it is infant formula]{modelName="food-price-history" slug="prince-edward-island-infant-formula-900-grams"} that has nearly doubled. :FoodPriceSpan[Vegetable oil]{modelName="food-price-history" slug="canada-vegetable-oil-3-litres"} and :FoodPriceSpan[canola oil]{modelName="food-price-history" slug="canada-canola-oil-3-litres"} seem to be up across the board, while :FoodPriceSpan[strawberries]{modelName="food-price-history" slug="canada-strawberries-454-grams"} are up overall but bounce faster than bitcoin. A select handful of items have actually :FoodPriceSpan[dropped in price]{modelName="food-price-history" slug="canada-chicken-drumsticks-per-kilogram"} since 2017, though the difference is highly dependent on where you live.

The data come from [Stats Canada](https://doi.datacite.org/dois/10.25318%2F1810024501-eng), which has been tracking the average price of 110 food items across every province since 2017. The table below shows a heatmap of price increases over the last six years: the shading of each point represents the overall change in price compared to the same month the previous year.

::FoodPriceTable{modelName="food-price-history" query="region=Canada&limit=12"}

The profile of nearly every product (and region) is unique. One common theme that emerges however is that the most significant increases happened in late 2021 and early 2022. That is, for items that increased the most, that increase more often than not came in the later stages of the pandemic.

::FoodPricePopover