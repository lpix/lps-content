---
title: "A visual breakdown of demographic changes to come in this century"
summary: "The world's population structure will change massively"
image: "world-demo"
date: "2023-10-25"
---

One of the most predictable trends in the ecology of humans is that as countries become wealthier, fertility rates decline. This means that even as mortality rates plummet and most people can expect to live into old age, the population of a country stabilizes and may even begin to shrink.

The population of the world has been increasing exponentially for centuries. [This period is now at an end](https://ourworldindata.org/world-population-growth-past-future). It is possible that within the lifetime of many people reading this, the world population may actually peak and begin to decline. Exponential growth being what is is, there is also considerable uncertainty about how exactly this will play out.

::DemographicScenarios

Note that the *difference* between these projections, by 2100, is close to the entire current population of the world.

For those of us concerned about the finitude of the planet we live on, this seems an unaccountable good. We simply cannot continue adding more and more billions of humans, forever. It *also* means that the transitory period is going to open new challenges. As the population continues to age and doesn't fully replace itself, the fraction of people past retirement age will skyrocket. 

This can be seen in the interactive below. Red shaded areas indicate ages that will make up a lower share of the population in the target year than they do now, while green areas indicate a higher share than they do now.

::DemographicChartControllable{modelName="un-pop-data" slug="medium_900"}

The small chart at the bottom shows how the dependency ratio (the ratio of people outside the workforce to inside it) will change over time. For comparison, present-day Florida (considered the retirement capital of the US) has a dependency ratio of 0.68. That's right: the world in 2100 will have a *higher* dependency ratio than present-day *Florida*, even under the highest fertility projection. You can click on or drag the bottom chart to change the target year.

Higher income countries trend towards even higher demographic ratios.

::DemographicChartControllable{modelName="un-pop-data" slug="medium_124,medium_840,medium_908"}

Japan presently has a famously elderly population. Its dependency ratio will continue to get even higher, before finally stabilizing at just above 1 (meaning more dependents than members of the workforce).

::DemographicChartControllable{modelName="un-pop-data" slug="medium_392"}

The bright spot, of course, will be countries that are currently developing, which will see their dependency ratios *decline* as their fertility rates decrease as their pyramid-shaped demographic curves change to include many more workers.

::DemographicChartControllable{modelName="un-pop-data" slug="medium_108,medium_887"}

You can explore the data further and look at your country or region by using bottom search box.

---

*Data hosted by the [United Nations](https://population.un.org/wpp/) and processed in this [Observable Notebook](https://observablehq.com/d/5477471317844e76). Inspired  by [Our World in Data](https://ourworldindata.org/explorers/population-and-demography).*
