---
title: "On React's Barrier's to Entry"
date: "2018-11-01"
---

In [React is the new Dojo](https://medium.com/@mikeal/react-is-the-new-dojo-18bd9059378f), Alex Russell argues that React's days as king among front-end frameworks are limited if it doesn't increase its circle of appeal outside of specialists who have the patience to learn all of the tooling that comes with it.

As he puts it:

> The Web didn’t win because it was the favorite among enterprises and “real developers.” It won because it scaled down better than it scaled up. It won because it amateurized software development and unlocked the creativity of an entire generation of programmers that couldn’t participate in software until The Web.

Of all the holy wars in the web community, this has been one of the hardest for me. On the one hand, I was attracted to coding for the web in the first place because of the openness of the platform, and tools like WordPress and jQuery were invaluable to me in getting literally dozens of sites up and running in those early days. In otherwords, I'm exactly the type of developer Alex is talking about.

On the other hand, my mindset has involved as I've become a more confident programmer. Once I became familiar with MVC patterns, WordPress felt ... awful to work with. So many of the "tricks" that I'd accepted as a normal part of WordPress development revealed themselves as clear anti-patterns. Similarly, once I learned to program the DOM with declarative rather than procedural code, doing anything in jQuery felt like rolling a stone up a hill. With these transitions came the realization that plugins aren't always necessary. WordPress plugins are opinionated and tie down your entire stack from database to design with a particular way of doing things. In my experience, clients and stakeholders and never happy with a plugin solution: it gets the job done, but not in quite the way they expect.

The question becomes: how much of the still-widespread use of WordPress and jQuery is due to developers being comfortable in those environments, versus the widespread plugin ecosystem that surrounds each one already. And if the chore of building plugins the old way is arduous enough, the community will simply move on.

In other words, one of the ways that jQuery "amateurized" software development was by standing on the shoulders of giants. How much longer will the giants be around for?

Nevertheless, Alex is right about one thing: React requires a barrier to entry to get started with, that dropping a script tag on a page does not. Or does it? As Dan Abromov points out, compiling JSX need not be any harder running a CSS preprocessor (a barrier to entry that most frontend developers are already employing). [React itself can be included on a page via a simple script tag, just like jQuery](https://twitter.com/dan_abramov/status/1011995700748521477)

I tried these two steps together and was able to get a React live on a page without using Webpack, Parceljs or anything else. In the example below, the "Today" button is rendered with JavaScript and activates an event when clicked, that fills the date input with the current date. Here is the code:

```html
<!DOCTYPE html>
<html>
  <body>
    <input type="date" id="date-input" />

    <div id="date-picker"></div>
    <script
      type="text/javascript"
      src="https://unpkg.com/react@16.0.0/umd/react.production.min.js"
    ></script>
    <script
      type="text/javascript"
      src="https://unpkg.com/react-dom@16.0.0/umd/react-dom.production.min.js"
    ></script>
    <script
      type="text/javascript"
      src="https://momentjs.com/downloads/moment.js"
    ></script>
    <script type="text/javascript" src="/index.js"></script>
  </body>
</html>
```

```js
function setDateInput(date) {
  document.getElementById("date-input").value =
    moment(date).format("YYYY-MM-DD");
}

ReactDOM.render(
  <button
    type="button"
    className="button"
    onClick={() => setDateInput(new Date())}
  >
    Today
  </button>,
  document.getElementById("date-picker")
);
```

```
{
  "name": "react-dropin",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "build": "npx babel --watch src --out-dir . --presets react-app/prod"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "babel-cli": "^6.26.0",
    "babel-preset-react-app": "^3.1.2"
  }
}
```
