# node-ncbi

This is a generic Node wrapper for the NCBI eUtils API. It is useful for building web-based tools, Node-based bioinformatics workflows, or displaying a publications list on an academic website.

Full documentation is available on GitHub.