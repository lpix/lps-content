# Hypocube

Hypocube is an attempt to encode some of my approach to data-vis into a React library. The [docs can be read here](https://hypocube.space/). A version of Hypocube (ported to Preact) is used as the basis for most of the visualizations on this website.